int[] cancelEven(int[] ring) {
    int numberOfSurvivors = ring.length / 2;
    int[] survivor = new int[numberOfSurvivors];
    for(int i = 0; i < numberOfSurvivors; i++)
        survivor[i] = ring[2*i];
    return survivor;
}

int[] cancelOdd(int[] ring) {
    int numberOfSurvivors = ring.length / 2 + 1;
    int[] survivor = new int[numberOfSurvivors];
    for(int i = 1; i < numberOfSurvivors; i++)
        survivor[i] = ring[2*(i-1)];
    survivor[0] = ring[ring.length - 1];
    return survivor;
}

int theWinnerIs(int[] ring) {
    if(ring.length == 1) return ring[0];
    if(ring.length % 2 == 0) return theWinnerIs(cancelEven(ring));
    return theWinnerIs(cancelOdd(ring));
}

int[] getRing(int n) {
    int[] ring = new int[n];
    for(int i = 0; i < n; i++) ring[i] = i + 1;
    return ring;
}