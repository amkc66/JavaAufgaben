void printArray2D(int[] a, int w) {
  for(int i = 0; i < a.length; i++) {
    if(i > 0 && i % w == 0) printf("\n");
    printf("%2d ", a[i]);
  }
  printf("\n");
}

int[] test = {
  -1, 2, 0,
   3,-5, 6,
  -1, 0,-2
};
