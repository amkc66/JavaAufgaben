int rollDoubles() {
  Random r = new Random();
  int x,y;
  int i = 0;
  do {
    x = r.nextInt(6) + 1;
    y = r.nextInt(6) + 1;
    i++;
  } while (x != y);
  return i;
}

int x = 0;
int n = 1000;
for(int i = 0; i < n; i++) {
  x += rollDoubles();
}
printf("Durchschnittliche Würfe bis zum Pasch: %.3f", 1.0 * x / n); //  <1>