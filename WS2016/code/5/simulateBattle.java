class Unit {
  int number;
  String type;
  int damage;
  int hitpoints;
  Unit(String type, int damage, int hitpoints, int number) {
    this.type = type;
    this.damage = damage;
    this.hitpoints = hitpoints;
    this.number = number;
  }
  static Unit createBoxers(int n) {
    return new Unit("Boxer", 1, 1, n);
  }
  static Unit createPikemen(int n) {
    return new Unit("Pikeniere", 2, 1, n);
  }
  static Unit createCatapults(int n) {
    return new Unit("Katapulte", 5, 2, n);
  }
  int killCount(Unit enemy) {
    return Math.min(enemy.number, number * damage / enemy.hitpoints);
  }
  String toString(String color) {
    return number + " " + color + " " + type;
  }
}

int fighterSum(Unit[] army) {
  int sum = 0;
  for(Unit u: army) {
    sum += u.number;
  }
  return sum;
}
Unit randomUnitAlive(Unit[] army) {
  Random r = new Random();
  Unit selected = null;
  do {
    selected = army[r.nextInt(army.length)];
  } while ( selected.number == 0 );
  return selected;
}
void printArmy(Unit[] army) {
  for(Unit u : army) {
    if(u.number > 0) {
      printf("  %3d %10s\n", u.number, u.type);
    }
  }
}
void simulateBattle(Unit[] blue, Unit[] red) {
  printf("Blau:\n");
  printArmy(blue);
  printf("Rot:\n");
  printArmy(red);
  while(fighterSum(blue) != 0 && fighterSum(red) != 0) {
    Unit blueUnit = randomUnitAlive(blue);
    Unit redUnit = randomUnitAlive(red);
    printf("%s gegen %s:\n", blueUnit.toString("blaue"), redUnit.toString("rote"));
    int kcBlue = blueUnit.killCount(redUnit);
    int kcRed = redUnit.killCount(blueUnit);
    printf("  %3d %5s %10s sterben\n", kcBlue, "rote", redUnit.type);
    printf("  %3d %5s %10s sterben\n", kcRed, "blaue" , blueUnit.type);
    blueUnit.number -= kcRed;
    redUnit.number -= kcBlue;
  }
  if (fighterSum(blue) > 0) {
    printf("Blau gewinnt und behält:\n");
    printArmy(blue);
  } else if (fighterSum(red) > 0) {
    printf("Rot gewinnt und behält:\n");
    printArmy(red);
  } else {
    printf("Alle Einheiten wurden getötet. Es gibt keinen Gewinner.\n");
  }
}

Unit[] blue = {Unit.createPikemen(17), Unit.createBoxers(4)};
Unit[] red = {Unit.createCatapults(1), Unit.createBoxers(10)};
