class Triangle {
  Point p1;
  Point p2;
  Point p3;
  Triangle(Point p1, Point p2, Point p3) {
    this.p1 = p1;
    this.p2 = p2;
    this.p3 = p3;
  }
  Line[] getSides() {
    return new Line[]{
      new Line(p1,p2),
      new Line(p2,p3),
      new Line(p3,p1)
    };
  }
}