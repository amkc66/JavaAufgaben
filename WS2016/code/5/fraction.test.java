// testing fraction.java

boolean testIdentity() {
    Fraction f = new Fraction(1/2);
    return f != f.add(new Fraction(0));
}

Boolean[] test = {
    new Fraction(3).toString().equals("3"),
    new Fraction(-3).toString().equals("-3"),
    testIdentity(),
    new Fraction(3_000_000_000_000L).toString().equals("3000000000000"),
    new Fraction(1,3).toString().equals("1/3"),
    new Fraction(-1,3).toString().equals("-1/3"),
    new Fraction(1,-3).toString().equals("-1/3"),
    new Fraction(-1,-3).toString().equals("1/3"),
    new Fraction(7,3).toString().equals("2 1/3"),
    new Fraction(-7,3).toString().equals("-2 1/3"),
    new Fraction(7,-3).toString().equals("-2 1/3"),
    new Fraction(-7,-3).toString().equals("2 1/3"),
    new Fraction(3,4).add(new Fraction(1,4)).toString().equals("1"),
    new Fraction(3,4).add(new Fraction(0)).toString().equals("3/4"),
    new Fraction(0).add(new Fraction(3,4)).toString().equals("3/4"),
    new Fraction(0).add(new Fraction(0)).toString().equals("0"),
    new Fraction(3,4).add(new Fraction(1,4).neg()).toString().equals("1/2"),  
    new Fraction(3,4).sub(new Fraction(1,4)).toString().equals("1/2"),
    new Fraction(6,4).add(new Fraction(5,3)).toString().equals("3 1/6"),
    new Fraction(6,4).add(new Fraction(-5,3)).toString().equals("-1/6"),
    new Fraction(-6,4).add(new Fraction(-5,3)).toString().equals("-3 1/6"),
    new Fraction(-6,4).add(new Fraction(5,3)).toString().equals("1/6"),
    new Fraction(3,4).mul(new Fraction(5,4)).toString().equals("15/16"),
    new Fraction(3,4).mul(new Fraction(5,4).inv()).toString().equals("3/5"),
    new Fraction(3,4).div(new Fraction(5,4)).toString().equals("3/5"),
}

printf("%d tests: discovered %d error(s)!\n",
    test.length,
    Arrays.stream(test).filter(n -> n == false).count());
