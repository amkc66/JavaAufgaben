class Medium {
  String title;
  int dur;
  Medium(String title, int dur) {
    this.title = title;
    this.dur = dur;
  }
  void play() {
    printf("Playing %s\n", title);
  }
  int getDurationSeconds() {
    return dur;
  }
}

class Video extends Medium {
  boolean subtitles = false;
  Video(String title, int dur) {
    super(title, dur);
  }
  void setSubtitles(boolean on) {
    subtitles = on;
  }
}

class Album extends Medium {
  String[] tracks;
  int[] trackDurations;
  int currentTrack = 0;
  Album(String title, String[] tracks, int[] trackDurations) {
    super(title, 0);
    this.trackDurations = trackDurations;
    for(int d: trackDurations) { this.dur += d; }
  }
  void play() {
    super.play();
    printf("Current track: %s\n", tracks[currentTrack]);
  }
  void nextTrack() {
    currentTrack = (currentTrack + 1) % tracks.length;
  }
}