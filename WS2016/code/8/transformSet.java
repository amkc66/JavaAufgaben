void printAll(int[] ar) {
  for(int x : ar) {
    printf("%d ", x);
  }
  printf("\n");
}

int [] randomDistinct(int n) {
  Random r = new Random();
  int count = 0;
  int [] ar = new int[n];
  while ( count < n) {
    int ri = r. nextInt (100);
    // find out if array already contains this value
    boolean contains = false ;
    for(int i = 0; i < count ; i++) {
      if( ar [i] == ri) {
        contains = true;
        break;
      }
    }
    if( !contains ) {
      ar[count++] = ri;      
    }
  }
  return ar;
}