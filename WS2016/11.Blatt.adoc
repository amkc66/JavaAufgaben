= 11. Woche: OO-Aufgaben
Dominikus Herzberg, Nadja Krümmel, Christopher Schölzel
V0.21, 2017-01-29
:toc:
:icons: font
:solution:
:source-highlighter: coderay
:sourcedir: ./code/
:imagedir: ./images

== Beispiele für Fragen und Korrektur-Aufgaben

### Bugfixing mit Ausdrücken (4 Punkte)

Korrigieren Sie die folgenden Java-Ausdrücke und -Anweisungen, so dass das erwartete Ergebnis entsteht:

* `1 / 5`
+
Erwartetes Ergebnis: `0.2`

* `"Summe = " + 10 + 5 + 3`
+
Erwartetes Ergebnis: `"Summe = 18"` -- Sie dürfen weder die Operatoren noch die Literale verändern.

* `10 = 4`
+
Erwartetes Ergebnis: `false`

* `false && false || true`
+
Erwartetes Ergebnis: `false` -- Sie dürfen weder die Operatoren noch die Literale verändern.

ifdef::solution[]
.Lösung

* `1.0 / 5`
* `"Summe = " + (10 + 5 + 3)`
* `10 == 4`
* `false && (false || true)`
endif::solution[]

### Zuweisungen (3 Punkte)

Welche der folgenden Zuweisungen ist erlaubt?

* [ ] `int x = 10;`
* [ ] `int x = 10L;`
* [ ] `long x = 10;`
* [ ] `Long x = new Integer(10);`
* [ ] `Integer x = new Long(10);`
* [ ] `int x = new Integer(10);`

ifdef::solution[]
.Lösung
* [x] `int x = 10;`
* [ ] `int x = 10L;`
* [x] `long x = 10;`
* [ ] `Long x = new Integer(10);`
* [ ] `Integer x = new Long(10);`
* [x] `int x = new Integer(10);`
endif::solution[]

### Collections (3 Punkte)

Nennen sie jeweils zwei Klassen, die die folgenden Schnittstellen implementieren:

* `java.util.List`
* `java.util.Set`
* `java.util.Map`

ifdef::solution[]
.Lösung

* `java.util.ArrayList`, `java.util.LinkedList`
* `java.util.HashSet`, `java.util.TreeSet`
* `java.util.HashMap`, `java.util.TreeMap`
endif::solution[]

== Beispiele für Kurz-Aufgaben

=== Mischen (5 Punkte)
Schreiben Sie eine Methode `shuffle`, die ein Objekt vom Typ `Swappable` entgegennimmt und die Elemente dieses Objekts mischt (d.h. in eine zufällige Reihenfolge bringt).

[source,java]
----
include::{sourcedir}/11/Swappable.java[]
----

ifdef::solution[]
.Lösung
[source,java]
----
include::{sourcedir}/11/SwappableShuffle.java[]
----
endif::solution[]

=== Wölfe und Hasen (8 Punkte)
Ändern Sie den folgenden Code so ab, dass die beiden Klassen ein Interface implementieren, das möglichst viele ihrer Gemeinsamkeiten zusammenfasst. Geben sie auch eine Deklaration des Interfaces selbst an.
// TODO vllt muss man hier nochmal die Formulierung präzisieren

[source,java]
----
include::{sourcedir}/11/RabbitAndWolf.java[]
----

ifdef::solution[]
.Lösung
[source,java]
----
include::{sourcedir}/11/RabbitAndWolf_solution.java[]
----

Die Methoden, die ins Interface kommen, müssen mit dem Modifizierer `public` versehen werden, da Interface-Methoden immer als `public` gedacht werden.

Mehr als diese beiden Methoden dürfen aus den folgenden Gründen nicht ins Interface:

* Interfaces dürfen keine Felder enthalten (`weight` und `name`). Hierfür müsste man eine abstrakte Klasse definieren.
* Die Methode `eat` in der Klasse `Rabbit` hat eine andere Signatur als in der Klasse `Wolf`. Damit handelt es sich aus Sicht des Java-Compilers um zwei völlig separate Methoden, die nichts gemeinsam haben.
* Konstruktoren können ebenfalls nicht in einem Interface definiert werden. Auch für einen gemeinsamen Konstruktor bräuchte man also eine abstrakte Klasse.
endif::solution[]

### RPG-Charakter (7 Punkte)
Schreiben Sie eine Klasse `RPGChar`, die einen Charakter in einem Computerrollenspiel darstellt. Die Klasse soll die folgenden Felder und Methoden besitzen sowie einen sinnvollen Konstruktor.

Felder:

* `name`: Der Name des Charakters.
* `strength`: Die Stärke des Charakters.
* `hitpoints`: Die Lebenspunkte des Charakters.

Methoden:

* `attack(RPGChar other)`: Greift einen anderen Charakter an. Dieser verliert dadurch Lebenspunkte in der Höhe der Stärke des Angreifers.
* `heal()`: Heilt den Charakter um 10 Lebenspunkte. Dabei darf das Maximum von 100 Lebenspunkten aber nicht überschritten werden.

ifdef::solution[]
.Lösung
[source, java]
----
include::{sourcedir}/11/RPGChar.java[]
----
endif::solution[]

### Benutzereingaben (4 Punkte)
Schreiben Sie ein kurzes Codefragment, das eine vom Benutzer eingegebene Zahl mit Hilfe eines `Scanner`-Objekts einliest und in einer Variable `x` speichert. Wenn die Konvertierung der Benutzereingabe fehlschlägt, soll `x` stattdessen den Wert `1` bekommen.

////
// Diese Warnung scheint mit aktuellen Releases nicht mehr nötig zu sein! (2017-03-29)
[WARNING]
====
In der JShell funktioniert die Klasse `java.util.Scanner` leider nicht richtig. Um Ihre Lösung für diese Aufgabe zu testen, müssen Sie den Code also in eine Main-Methode innerhalb einer Klasse verpacken und mit dem Java-Compiler `javac` kompilieren.
====
////

ifdef::solution[]
.Lösung
[source, java]
----
include::{sourcedir}/11/ParseCatch.java[]
----
endif::solution[]

### Rekursives Modulo (6 Punkte)
Implementieren Sie eine rekursive Methode `mod`, die zwei positive ganze Zahlen `a` und `b` übernimmt und den Rest der Ganzzahldivision von `a` durch `b` errechnet.

Sie dürfen weder den Modulo-Operator `%` noch den Divisionsoperator `/` noch die Schlüsselwörter `for` oder `while` verwenden.

ifdef::solution[]
.Lösung
[source, java]
----
include::{sourcedir}/11/Mod.java[]
----
endif::solution[]

### Schleifen (8 Punkte)

Nennen Sie vier verschiedene Schleifentypen in Java. Demonstrieren Sie für jeden Typ, wie mit einer solchen Schleife die Elemente des folgenden Arrays summiert werden können.

`int[] ar = {1,5,34,6,1,5,6,1,6};`

ifdef::solution[]
.Lösung

* `for`-Schleife
+
[source, java]
----
int sum = 0;
for(int i = 0; i < ar.length; i++) {
  sum += ar[i];
}
----
* `while`-Schleife
+
[source, java]
----
int sum = 0;
int i = 0;
while(i < ar.length) {
  sum += ar[i];
  i++;
}
----
* `do/while`-Schleife
+
[source, java]
----
int sum = 0;
int i = 0;
do {
  sum += ar[i];            // <1>
  i++;
} while (i < ar.length);
----
<1> Hier würde einem Array der Länge `0` eine `IndexOutOfBoundsException` geworfen, während bei allen anderen Schleifentypen in diesem Fall die Schleife gar nicht erst betreten wird.
* `for`each-Schleife
+
[source, java]
----
int sum = 0;
for(int x: ar) {
  sum += x;
}
----
endif::solution[]

== Skizze für Programmieraufgaben in Klausur

=== Kommentare in Textdatei löschen
Schreiben Sie ein Programm, das eine Textdatei einliest, alle Kommentare daraus entfernt und das Resultat in eine Datei schreibt. Der Name der einzulesenden Datei und der zu schreibenden Datei sind als Argumente an das Programm zu übergeben.

==== 1. Variante: `#` leitet Kommentar ein (7 Punkte)
Es handele sich um ein Textformat, bei dem folgende Konvention für Kommentare gilt: Wenn das erste Zeichen in der Zeile ein `#` (Doppelkreuz) ist, dann ist die gesamte Zeile eine Kommentarzeile.

ifdef::solution[]
.Lösung
[source, java]
----
include::{sourcedir}/11/CommentDeleter.java[]
----
endif::solution[]

==== 2. Variante: Java-Kommentare löschen (10 Punkte)
Passen Sie das Programm an für die Verarbeitung von Java-Programmtext. Es soll jegliche Java-Kommentare löschen.

==== 3. Variante: Java-Kommentare durch Leerzeichen ersetzen (13 Punkte)
Passen Sie das Programm an für die Verarbeitung von Java-Programmtext. Kommentare werden durch Leerzeichen ersetzt, so dass sich für den übrig gebliebenen Quelltext nichts ändert, was die Zeilennummer oder die Position eines Programmtextes in einer Zeile angeht.

=== Versions-Aktualisierung

Entwickeln Sie ein Programm, das die Informationen zur Versionierung in einer Textdatei aktualisiert. Die Textdatei wird als Argument beim Programmaufruf übergeben.

==== 1. Variante: Hochzählen einer Versionsnummer (7 Punkte)
Dem Programm zur Versions-Aktualisierung wird beim Aufruf der Name einer Textdatei übergeben. Der Aktualisierer sucht in der gegebenen Textdatei nach einer Zeile, die an beliebiger Stelle eine Versionsangabe im Format `Version 13.` enthält. Geben Sie den Dateitext vollständig auf der Konsole aus, wobei die Versionsnummer links vom Punkt hochgezählt wird: aus `Version 13.` wird `Version 14`, aus `Version 99.` wird `Version 100.` usw.

ifdef::solution[]
.Lösung
[source, java]
----
include::{sourcedir}/11/VersionUpdater.java[]
----
endif::solution[]

==== 2. Variante: Subversionsnummer & Datum berücksichtigen (15 Punkte)
Dem Programm zur Versions-Aktualisierung wird beim Aufruf der Name einer Textdatei übergeben. Der Aktualisierer sucht in einer gegebenen Textdatei nach einer Zeile, die neben einer Versionsangabe im Format `V1.3` ein Datum im Format `YYYY-MM-DD` hat. Geben Sie auf der Konsole die Datei vollständig aus, wobei das Datum tagesaktuell ersetzt und die Versionsnummer rechts vom "Komma" hochgezählt wird. Dieser Anteil der Versionsnummer wird auf drei Stellen genau gezählt, wobei Nullen nach rechts "abgeschnitten" werden. Ein paar Beispiele: Aus `.0` wird `.001`, aus `.1` wird `.101`, aus `.099` wird `0.1`, aus `.009` wird `.01`. Nur bei `.999` wird auch die Zahl links vom Komma hochgezählt, so wird aus `7.999` die Version `8.0`.

=== Geschwindigkeitsbegrenzung (8 Punkte)
Vervollständigen Sie die folgende Implementierung der Klasse `Car` um eine Fehlerbehandlung:

* Ein Auto kann nicht beliebig schnell beschleunigen. Für `amount` sind daher nur positive Werte bis maximal `10` erlaubt. Falsche Eingaben sollen in einer Ausnahme vom Typ `IllegalArgumentException` resultieren.
* Ein Auto kann auch nicht beliebig schnell werden. Sobald die Variable `speed` den Wert `300` erreichen oder überschreiten würde, soll eine `TooFastException` geworfen werden. Implementieren Sie diese _geprüfte_ Ausnahme selbst.

Übergeben Sie den geworfenen Ausnahmen in beiden Fällen eine sinnvolle Nachricht, die dem Benutzer anzeigt, welches Problem entstanden ist. Achten Sie auch darauf, dass die Variable `speed` sich bei einem fehlerhaften Aufruf nicht ändert.

[source, java]
----
include::{sourcedir}/11/Car.java[]
----

ifdef::solution[]
.Lösung
[source, java]
----
include::{sourcedir}/11/Car_solution.java[]
----
endif::solution[]
